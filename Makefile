OBJ = main.o
TARGET = main
COMPILEOPT = -Wall -Wno-sign-compare -O2 -std=c++11 -DLOCAL -s -pipe -mmmx -msse -msse2 -msse3
vpath %.cpp ..
vpath %.h ..

.PHONY: all clean

all: $(TARGET)

$(TARGET): $(OBJ)
	g++-7 -o $@ $(OBJ)

%.o: %.cpp main.cpp
	g++-7 $(COMPILEOPT) -c $<

clean:
	rm -f $(TARGET)
	rm -f $(OBJ)
