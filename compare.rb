def read(file_name)
	res = []
	seed = 0
	IO.foreach(file_name) do |line|
		if line =~ /seed :(\d+)/
			seed = $1.to_i
		end
		if line =~ /score:\s*(\d+)/
			score = $1.to_i
			res[seed] = score
		end
	end
	return res
end

res1 = read(ARGV[0])
res2 = read(ARGV[1])
sum = 0
win = 0
lose = 0
tie = 0
(0..res1.size).each do |seed|
	next unless res1[seed] && res2[seed]
	diff = res2[seed] - res1[seed]
	win += 1 if diff > 0
	tie += 1 if diff == 0
	lose += 1 if diff < 0
	sum += diff
	printf("%3d %7d\n", seed, diff)
end
puts "win :#{win}"
puts "lose:#{lose}"
puts "tie :#{tie}"
puts "ave :#{1.0 * sum / (win + tie + lose)}"
puts

