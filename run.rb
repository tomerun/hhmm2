require 'parallel'
require "open3"

seeds = (100...350).to_a
results = Parallel.map([seeds[0...(seeds.size/2)], seeds[(seeds.size/2)..-1]]) do |ss|
	ss.map do |seed|
		`./main < testcase/#{seed}.in > log/res_#{seed}.txt`
		stdout, stderr, status = Open3.capture3("problem2_toolkit_JP/scripts/evaluator testcase/#{seed}.in log/res_#{seed}.txt")
		stdout =~ /Add.*?(\d+) points.*?Loss.*?(\d+) points.*?Bonus.*?(\d+) points/m
		add = $1.to_i
		loss = $2.to_i
		bonus = $3.to_i
		[seed, add, loss, bonus, add - loss + bonus + 5000]
	end
end.flatten(1).sort

results.each do |r|
	printf("seed :%3d\n", r[0])
	printf("add  :%5d loss :%5d bonus:%d\n", r[1], r[2], r[3])
	printf("score:%6d\n", r[4])
	puts
end
puts 'ave:' + (1.0 * results.sum {|r| r[-1]} / seeds.size).to_s
